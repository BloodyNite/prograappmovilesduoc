# PrograAppMovilesDuoc
## Repo del proyecto basado en el documento Ionic_03BasesIonic.pdf

Instrucciones

1. En la terminal pegar `git clone https://gitlab.com/BloodyNite/prograappmovilesduoc.git`
2. Desde la terminal ubicarse dentro de la carpeta del proyecto deseado
3. Ejecutar `npm install` para instalar las librerias necesarias
4. Levantar con `ionic serve` para ionic o `ng serve` si es proyecto de angular
