import { Component, OnInit } from '@angular/core';

interface Componente{
  icon: string;
  name: string;
  redirecTo: string;
}

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.page.html',
  styleUrls: ['./inicio.page.scss'],
})
export class InicioPage implements OnInit {

  componentes: Componente[] = [
    {
      icon: 'alert-circle',
      name: 'alert',
      redirecTo: '/alert'
    },
    {
      icon: 'document-text',
      name: 'action-sheet',
      redirecTo: '/action-sheet'
    },
  ];

  constructor() { }

  ngOnInit() {
  }

  onClick(){

  }

}
